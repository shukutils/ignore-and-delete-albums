import argparse
from datetime import date
import os
import shutil

VERSION = 0.2

def is_true(s):
    """
    checks the string s for truth - see:
    https://stackoverflow.com/questions/715417/converting-from-a-string-to-boolean-in-python
    """
    return s.lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh', 'ja']

def ask_for_root():
    """
    asks for the basepath where your music is stored and checks if it's a valid folder.
    """
    print("Assuming directory structure of /music_folder/ARTIST/YEAR - ALBUM/TRACK - ARTIST - TITLE")
    root = input("Enter root directory of your music library (e.g \"/home/USER/Music\"): ")
    while not os.path.isdir(root):
        print("It seems, the directory does not exist or you don't have enough permissions to access it.")
        root = input("Enter root directory of your music library (e.g \"/home/USER/Music\"): ")
    return root

def get_dirs(path):
    """
    returns a list of only subdirectories of path, excluding files.
    """
    dirs = [l for l in os.listdir(path) if os.path.isdir(os.path.join(path, l))]
    return dirs

def get_tracks(path):
    """
    get all tracks ending with ogg or mp3 in path.
    """
    return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (f.endswith('ogg') or f.endswith('mp3'))]

def relevant_path(path):
    """
    takes absolut path like "/path/to/music/artist/album/" and returns "artist/album".
    """
    path_list = path.split(os.sep)
    return os.path.join(path_list[-2], path_list[-1])


def parse_albums(albums, config):
    """
    parse albums (as paths) and ignores them based on config.
    """
    rara_hates_it = {}
    for path in albums:
        path = os.path.normpath(path)

        num_tracks = len(get_tracks(path))

        if config.ignore_less_songs_than and num_tracks < config.ignore_less_songs_than:
            rara_hates_it[path] = 'single: {} songs'.format(num_tracks)

        elif config.ignore_instrumental and 'instrumental' in path.lower():
            rara_hates_it[path] = 'instrumental'

    return rara_hates_it

def delete_and_ignore(root, paths, delete=False, exclude_filename='00-Sync-Config/auto_excluded_rara.txt'):
    """
    actually writes the ignore-file and deletes (optionally) what is ignored from local folder.
    """
    try:
        with open(os.path.join(root, exclude_filename), 'w') as f:
            f.write('// This is an automatically generated ignore file.\n')
            f.write('// Manual changes to this file WILL BE OVERWRITTEN!\n')
            f.write('// \n')
            f.write('// It was created with the delete_and_ignore_albums.py script (version {}) from shukutils.\n'.format(VERSION))
            f.write('// Last recreation was {}.\n'.format(date.today()))
            f.write('// \n')
            f.write('// Note: you need to include this file in YOUR .stignore file.\n')
            f.write('//       Your .stignore file will NEVER be synced, but this file will.\n')
            f.write('//       That is a good thing.\n')
            f.write('//       If you want to include things, that are excluded by this file\n')
            f.write('//       (e.g. a specific single you actually like), just have it as an include\n')
            f.write('//       statement (preceded with a !) BEFORE you include this file.\n')
            f.write('//       So your .stignore file could look like this:\n')
            f.write('// \n')
            f.write('// ------------ .stignore content: ------------\n')
            f.write('// !/Juse Ju/2020 - Kranichkick\n')
            f.write('// !/Fatoni/2019 - Andorra (Instrumental)\n')
            f.write('// /Juse Ju/2020 - Millenium\n')
            f.write('// \n')
            f.write('// #include {}\n'.format(exclude_filename))
            f.write('// --------------------------------------------\n')
            f.write('// \n')
            f.write('// This will NOT ignore Kranichkick and Andorra, even though they are singles/instrumental and later listed\n')
            f.write('// in {}. But this WILL ignore Millenium, even though it is not the included file.\n'.format(exclude_filename))
            f.write('// Manually edited values in .stignore will not be deleted by this script.\n')
            f.write('// Good luck.\n')
            f.write('// \n')
            f.write('// \n')
            new_dict = {}
            for k, v in paths.items():
                new_dict.setdefault(v, []).append(k)
            for comment, path_list in new_dict.items():
                f.write('// {}\n'.format(comment))
                for path in path_list:
                    part_path = '/{}'.format(relevant_path(path).strip('/'))
                    f.write('{}\n'.format(part_path))
                    if delete:
                        print("Ignore and delete \"{}\".".format(part_path))
                        shutil.rmtree(path, ignore_errors=True)
                    else:
                        print("Ignore \"{}\", but keep local copy.".format(part_path))
    except:
        raise

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process music library.')
    parser.add_argument('path_to_music_lib', type=str,
                        help='path to your music folder (expecting structure:\npath_to_music_lib/ARTIST/ALBUM)')
    parser.add_argument('--ignore_less_songs_than', type=int, default=0,
                        dest='ignore_less_songs_than',
                        help='ignore every album with less songs than (default: 0)')
    parser.add_argument('--ignore_instrumental', dest='ignore_instrumental', action='store_true',
                        help='ignore instrumental albums')
    parser.add_argument('--delete', dest='delete', action='store_true',
                        help='delete ignored albums')

    config = parser.parse_args()

    artists = get_dirs(config.path_to_music_lib)

    albums = []
    for artist in artists:
        albums.extend([os.path.join(config.path_to_music_lib, artist, album) for album in get_dirs(os.path.join(config.path_to_music_lib, artist))])

    rara_hates_it = parse_albums(albums, config)
    delete_and_ignore(config.path_to_music_lib, rara_hates_it, delete=config.delete)
