Use this script to delete and ignore unwanted elements from your music library. Very specific usecase.

Example:
`python3 ignore_and_delete_albums.py /dein/pfad/zur/musik --ignore_less_songs_than 3 --ignore_instrumental`
and to also locally delete ignored albums:
`python3 ignore_and_delete_albums.py /dein/pfad/zur/musik --ignore_less_songs_than 3 --ignore_instrumental --delete`

Relevant documentation:
 - https://docs.syncthing.net/users/ignoring.html
 - https://forum.syncthing.net/t/how-do-i-ignore-folders-and-files/15004/8
